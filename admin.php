<?php  
include("config.php");

session_start();

// Additional protection : 
// if user is not admin and tries to access AdminPanel with URL anyway, redirected to login page
if ($_SESSION["isAdmin"]==0) header("location: index.php");


// If user filled in ADD form
if (!empty($_POST['add'])) {

	// Protection against SQL Injections (see index.php)
	$usr = mysqli_real_escape_string($db,$_POST['username']);
	$pass = mysqli_real_escape_string($db,$_POST['password']);

	$isAdmin=$_POST['isAdmin'];

	// Updates SQL request with admin boolean
	$sqlAdd = "INSERT INTO `PhotoShare`.`Users` (`username`, `password`, `isAdmin`) VALUES ('$usr', '$pass', ";
	if ($isAdmin==1) {
		$sqlAdd .= " 1)";
	}
	else {
		$sqlAdd .= " 0)";
	}
	$result = mysqli_query($db,$sqlAdd) or die("Query error");
}

// If user filled in REMOVE form
if (!empty($_POST['rem'])) {

	// Runs SQL request to remove selected user from the database
	$remove = mysqli_real_escape_string($db,$_POST['remove']);
	$sqlRemove = "DELETE FROM `PhotoShare`.`Users` WHERE `Users`.`userID` = $remove";
	$result2 = mysqli_query($db,$sqlRemove) or die("Query error");
}

?>

<!doctype html>

<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin</title>



	<link href='//fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.indigo-pink.min.css">
	<link rel="stylesheet" href="styles.css">

	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">

</head>

<body>
  <header>
   <ul class="w3-navbar w3-margin-bottom w3-blue-grey w3-border w3-large">
    <li><a class="w3-green" href="/profile.php"><i class="fa fa-home w3-large"></i></a></li>
    <li><a href="/upload.php"><i class="fa fa-cloud-upload w3-large"></i></a></li>
    <li><a href="/members.php"><i class="fa fa-group w3-large"></i></a></li>
    <?php session_start(); if ($_SESSION["isAdmin"]==1) echo "<li><a href=\"/admin.php\"><i class=\"fa fa-gears w3-large\"></i></a></li>"; ?>
    <li><a href="/index.php"><i class="fa fa-sign-in w3-large"></i></a></li>
    <li class="w3-right w3-small"><div class="w3-container"><p><?="Welcome, ". $_SESSION["username"]?></p></div></li>
  </ul>
</header>

	<div class="w3-container">
		<div class="w3-container w3-pale-blue w3-center w3-round-xlarge w3-margin-bottom"><h3>Administration Panel</h3></div>
		<div class="w3-container w3-quarter"><p> </p></div>


		<div class="w3-card w3-pale-blue w3-quarter">

			<div class="w3-container w3-center w3-green">
				<h3>Add User</h3>
			</div>

			<form class="w3-container w3-center" method="POST">

				<input class="w3-input" type="text" id="username" name="username">
				<label>Username</label>
				<input class="w3-input" type="password" id="password" name="password">
				<label>Password</label>

				<p>
					<input class="w3-radio" type="radio" name="isAdmin"
					<?php 
					// Displays only one checked item at a time 
					if (isset($isAdmin) && $isAdmin=="0") echo "checked";?>
					value="0">User
					<input class="w3-radio" type="radio" name="isAdmin"
					<?php if (isset($isAdmin) && $isAdmin=="1") echo "checked";?>
					value="1">Admin

				</p>
				<input class="w3-btn w3-green w3-border" type="submit" value="Add User" name="add">
			</form>
			<?php 
			// If user added successfully, display message
			if (!empty($_POST['add']) && ($result)) echo "<div class=\"w3-container w3-pale-green w3-bottombar w3-border-green w3-border\"> <p>New account successfully added to the database</p> </div>";?>

		</div>


		<div class="w3-card w3-pale-blue w3-center w3-quarter">

			<div class="w3-container w3-red">
				<h3>Remove User</h3>
			</div>

			<form class="w3-container" method="POST">


				<select class="w3-select w3-border w3-white w3-round-large" name="remove" id="remove">
					<option value="" disabled selected>Pick a user ...</option>
					<?php

					// Retrieves from database all the users 
					$qry = "SELECT * FROM `PhotoShare`.`Users`";
					$rslt= mysqli_query($db, $qry) or die("Query error");


					while($rows = mysqli_fetch_array($rslt, MYSQLI_ASSOC)){

						// Displays all available users as options in the dropdown list
						echo "<option value=".$rows["userID"].">".$rows["username"]."</option>";
					}
					?> 
				</select>
				<p>
					<input class="w3-btn w3-red w3-border" type="submit" value="Remove User" name="rem">
				</p>

			</form>
			<?php 
			// Displays success message
			if (!empty($_POST['rem']) && ($result2))echo "<div class=\"w3-container w3-pale-red w3-bottombar w3-border-red w3-border\"> <p>Account successfully removed from the database</p> </div>";?>

		</div>


	</div>
</body>


</html>