-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 08, 2016 at 08:47 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `PhotoShare`
--

-- --------------------------------------------------------

--
-- Table structure for table `Images`
--

CREATE TABLE IF NOT EXISTS `Images` (
  `imageID` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(15) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `date-time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `visibility` tinyint(1) DEFAULT NULL,
  `isFavorite` int(1) DEFAULT NULL,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `Images`
--

INSERT INTO `Images` (`imageID`, `name`, `username`, `path`, `date-time`, `visibility`, `isFavorite`) VALUES
(10, 'Nature-Photography-High-Quality-Wallp...', 'alex', './users/alex/Nature-Photography-High-Quality-Wallpapers.jpg', '2016-05-07 12:22:56', 0, 1),
(11, 'Ajay-Thakur.jpg', 'alex', './users/alex/Ajay-Thakur.jpg', '2016-05-07 12:23:01', 0, 0),
(12, 'underwater-jellyfish-sun-ofu-20120114_2245.jpg', 'alex', './users/alex/underwater-jellyfish-sun-ofu-20120114_2245.jpg', '2016-05-07 12:23:07', 0, NULL),
(13, 'maxresdefault.jpg', 'alex', './users/alex/maxresdefault.jpg', '2016-05-07 12:23:21', 0, NULL),
(15, 'escapefromreality-copy.jpg', 'alex', './users/alex/escapefromreality-copy.jpg', '2016-05-07 21:38:27', 1, NULL),
(18, 'persist3.jpg', 'alex', './users/alex/persist3.jpg', '2016-05-07 22:55:35', 0, NULL),
(24, 'underwater_jelly_fish.jpg', 'Sophie', './users/Sophie/underwater_jelly_fish.jpg', '2016-05-08 16:22:58', 1, 1),
(25, 'persist3.jpg', 'Sophie', './users/Sophie/persist3.jpg', '2016-05-08 16:27:07', 1, NULL),
(26, 'forest.jpg', 'Sophie', './users/Sophie/forest.jpg', '2016-05-08 16:28:17', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `userID` tinyint(3) NOT NULL AUTO_INCREMENT COMMENT 'PrimaryKey for Users',
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  `profilePic` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`userID`, `username`, `password`, `isAdmin`, `profilePic`) VALUES
(2, 'alex', 'alex', 1, NULL),
(11, 'user', 'user', 0, NULL),
(19, 'Sophie', 'sophie', 1, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
