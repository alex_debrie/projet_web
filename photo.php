<?php
session_start();
include("config.php");

// Retrieves image ID via GET method
$img = $_GET["id"];


$query = "SELECT * FROM `PhotoShare`.`Images` WHERE `imageID`=" . $img;
$result= mysqli_query($db, $query);
// Retrieves image full path
$path = mysqli_fetch_array($result, MYSQLI_ASSOC)["path"];
$name = basename($path);

// If user clicked on trash button
if (isset($_GET['del'])) {
// Remove image from database
  $sql = "DELETE FROM `PhotoShare`.`Images` WHERE `Images`.`imageID` =" . $img;
  if (mysqli_query($db, $sql) ) {
// If removal from db successful, deletes image file from server
    unlink($path); 
// Redirects to profile page after image has been deleted
    header("location: profile.php");
  }
}

?>

<!doctype html>

<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Photo</title>



  <link href='//fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.indigo-pink.min.css">
  <link rel="stylesheet" href="styles.css">

  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">

</head>

<body>
  <header>
   <ul class="w3-navbar w3-margin-bottom w3-blue-grey w3-border w3-large">
    <li><a class="w3-green" href="/profile.php"><i class="fa fa-home w3-large"></i></a></li>
    <li><a href="/upload.php"><i class="fa fa-cloud-upload w3-large"></i></a></li>
    <li><a href="/members.php"><i class="fa fa-group w3-large"></i></a></li>
    <?php
    // Retreives $SESSION variables and displays link to AdminPanel only if user is Admin 
    session_start(); 
    if ($_SESSION["isAdmin"]==1) echo "<li><a href=\"/admin.php\"><i class=\"fa fa-gears w3-large\"></i></a></li>"; 
    ?>
    <li><a href="/index.php"><i class="fa fa-sign-in w3-large"></i></a></li>
    <li class="w3-right w3-small"><div class="w3-container"><p><?="Welcome, ". $_SESSION["username"]?></p></div></li>
  </ul>
</header>

<div class="w3-container">
  <div class="w3-container w3-pale-blue w3-center w3-round-xlarge w3-margin-bottom">
    <div class="w3-left"> <a href="profile.php"><i class="fa fa-mail-reply w3-xlarge w3-margin-top"></i></a></div>
    <h3><?=$name?></h3>
  </div>

  <div class="w3-container w3-pale-blue w3-round-xlarge w3-padding-12">
    <div class="w3-container w3-threequarter w3-center">
      <?php

       // Displays selected image 
      echo "<img src=\"" . $path ."\" onclick=\"document.getElementById('modal01').style.display='block'\" style=\"width: 80%\" class=\"w3-hover-opacity\">

      <div id=\"modal01\" class=\"w3-modal w3-animate-zoom\" onclick=\"this.style.display='none'\">
        <img class=\"w3-modal-content\" src=\"".$path. "\">
      </div>";


      $e = exif_read_data($path, 0, true);
      ?>
    </div>

    <div class="w3-container-12 w3-quarter">
      <table class="w3-table w3-striped w3-bordered">
        <tr>
          <th>Property</th>
          <th>Value</th>
        </tr>
        <tr>
          <td>Manufacturer</td>
          <td><?=$e['IFD0']['Make']?></td>
        </tr>
        <tr>
          <td>Model</td>
          <td><?=$e['IFD0']['Model']?></td>
        </tr>
        <tr>
          <td>Exp Time</td>
          <td><?=$e['EXIF']['ExposureTime']?></td>
        </tr>
        <tr>
          <td>Focal length</td>
          <td><?=$e['EXIF']['FocalLength']?></td>
        </tr>
        <tr>
          <td>ISO</td>
          <td><?=$e['EXIF']['ISOSpeedRatings']?></td>
        </tr>
        <tr>
         <td>Software</td>
         <td><?=$e['IFD0']['Software']?></td>
       </tr>
       <tr>
        <td>Size</td>
        <td><?=$e['COMPUTED']['Width']." x ". $e['COMPUTED']['Height']?> </td>
      </tr>
      <tr>
        <td>Copyright</td>
        <td><?=$e['IFD0']['Copyright']?></td>
      </tr>
    </table>

    <div class="w3-container w3-center w3-margin-top">
      <div class="w3-third">
        <?php
        // If visiting your own photo, displays trash button to delete it
        if ($_SESSION["profile"]=="own") echo "<a href=\"photo.php?id=" . $img . "&del=true\"><i class=\"fa fa-trash w3-xxlarge\"></i></a>";
        ?>
      </div>
      <div class="w3-third">

        <?php
        // If visiting your own photo, displays padlock to edit visibility setting
        if ($_SESSION["profile"]=="own"){
          if (isset($_GET["vis"])) $vis = $_GET["vis"];
          $q = "SELECT * FROM `PhotoShare`.`Images` WHERE `imageID`=" . $img;
          $r = "UPDATE `PhotoShare`.`Images` SET `visibility` =";

        // Edits the SQL request according 
          if ($vis==0) mysqli_query($db, $r . "'0' WHERE `Images`.`imageID` =" . $img);
          if ($vis==1) mysqli_query($db, $r . "'1' WHERE `Images`.`imageID` =" . $img);

        // Displays an open/closed padlock according to image visibility settings
          $v = mysqli_fetch_array(mysqli_query($db, $q), MYSQLI_ASSOC)["visibility"];
          if ($v == 1) echo "<a href=\"photo.php?id=". $img ."&vis=0\"><i class=\"fa fa-unlock w3-xxlarge\"></i></a>"; 
          if ($v == 0) echo "<a href=\"photo.php?id=". $img ."&vis=1\"><i class=\"fa fa-lock w3-xxlarge\"></i></a>";
        }
        ?>
        
      </div>
      <div class="w3-third">
        <?php
        // If visiting your own photo, displays star to edit Favorite
        if ($_SESSION["profile"]=="own"){
          if (isset($_GET["f"])) $f = $_GET["f"];
          $s = "SELECT * FROM `PhotoShare`.`Images` WHERE `imageID`=" . $img;
          $t = "UPDATE `PhotoShare`.`Images` SET `isFavorite` =";

        // Edits the SQL request according 
          if ($f==0) mysqli_query($db, $t . "'0' WHERE `Images`.`imageID` =" . $img);
          if ($f==1) mysqli_query($db, $t . "'1' WHERE `Images`.`imageID` =" . $img);

        // Displays an empty/full star according to favorite settings
          $fa = mysqli_fetch_array(mysqli_query($db, $s), MYSQLI_ASSOC)["isFavorite"];
          if ($fa == 1) echo "<a href=\"photo.php?id=". $img ."&f=0\"><i class=\"fa fa-star w3-xxlarge\"></i></a>"; 
          if ($fa == 0) echo "<a href=\"photo.php?id=". $img ."&f=1\"><i class=\"fa fa-star-o w3-xxlarge\"></i></a>";
        }
        ?>
      </div>
    </div>


  </div>
</div>
<div class="w3-card-4" id="exif" style="display: none">
  <?php
  foreach ($e as $key => $section_data) {
    foreach ($section_data as $name => $val) {
      print "$key.$name : $val \r\n";
    }
  }
  ?>
</div>
</body>
</html>