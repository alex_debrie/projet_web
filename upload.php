<!doctype html>

<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Upload</title>

	<link rel="stylesheet" href="styles.css">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">

</head>

<body>
  <header>
   <ul class="w3-navbar w3-margin-bottom w3-blue-grey w3-border w3-large">
    <li><a class="w3-green" href="/profile.php"><i class="fa fa-home w3-large"></i></a></li>
    <li><a href="/upload.php"><i class="fa fa-cloud-upload w3-large"></i></a></li>
    <li><a href="/members.php"><i class="fa fa-group w3-large"></i></a></li>
    <?php session_start(); if ($_SESSION["isAdmin"]==1) echo "<li><a href=\"/admin.php\"><i class=\"fa fa-gears w3-large\"></i></a></li>"; ?>
    <li><a href="/index.php"><i class="fa fa-sign-in w3-large"></i></a></li>
    <li class="w3-right w3-small"><div class="w3-container"><p><?="Welcome, ". $_SESSION["username"]?></p></div></li>
  </ul>
</header>


	<div class="w3-container">
		<div class="w3-container w3-center w3-pale-blue w3-round-xlarge w3-margin-bottom">
			<h3>Upload</h3>
		</div>

		<div class="w3-quarter"><p> </p></div>
		<div class="w3-card-12 w3-pale-blue w3-half w3-round-xlarge w3-padding">
			<p>Pick an image file from your computer and hit upload to publish it on your profile page.</p>
			<p>Bear in mind that maximum file size is fixed at 10MB.</p>

			<form method="post" name="fileToUpload" enctype="multipart/form-data">

				<input class="w3-btn w3-blue-grey w3-round-xlarge w3-small w3-margin-right" name="userfile" type="file"> 

				<input class="w3-input" type="text" name="imgname" placeholder="Rename the image...">

				<input class="w3-radio" type="radio" name="isPriv"
				<?php 
				// Allows only one checked item a a time
				if (isset($isPriv) && $isPriv=="0") echo "checked";?>
				value="0">
				Private
				<input class="w3-radio" type="radio" name="isPriv"
				<?php if (isset($isPriv) && $isPriv=="1") echo "checked";?>
				value="1">
				Public

				<input class="w3-right w3-btn w3-blue-grey w3-margin-bottom" name="upload" type="submit" id="upload" value=" Upload ">

				<?php 
				session_start();
				include("config.php");

				// If user clicked on upload
				if(isset($_POST['upload'] ))
				{
					// Retrieve file via FILES method
					$file = $_FILES['userfile'];

					$isPriv=$_POST['isPriv'];

					$rename = $_POST["imgname"];
					$tmpfile = $file['tmp_name'];

					// Create directory with appropriate permissions
					mkdir('users/' . $_SESSION["username"]);
					
					// Set the full path of the file 
					$dir = "./users/" . $_SESSION["username"];
					$dest = $dir . '/'; 

					// Use default name if no name provided by the user
					if (!(isset($_POST["imgname"]))) $dest .= basename($file['name']) ;
					elseif (isset($_POST["imgname"])) $dest .= $_POST["imgname"];

					// Add file to the database
					$query = "INSERT INTO `PhotoShare`.`Images` (
					`imageID` ,
					`name` ,
					`username` ,
					`path` ,
					`date-time` ,
					`visibility`
					)
					VALUES (
					NULL ,'". $_POST["imgname"] ."','" . $_SESSION["username"] . "' ,'". $dest ."',
					CURRENT_TIMESTAMP,";

					if ($isPriv==0) $query .= "'0')";
					if ($isPriv==1) $query .= "'1')";

					if(isset ($file) && ($file['error'] == 0) && ($file['size'] <= 10000000))
					{
						if ( (move_uploaded_file( $tmpfile , $dest)) && (mysqli_query($db,$query))) echo "<div class=\"w3-container w3-pale-green w3-bottombar w3-border-green w3-border\"> <p>New picture successfully added to the database !</p> </div>";
						else echo "<div class=\"w3-container w3-pale-red w3-bottombar w3-border-red w3-border\"> <p>Upload failed !</p> </div>";
					}
				}  

				?>



			</form> 
		</div>
	</div>

</body>

</html>