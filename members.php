<!doctype html>

<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Site Members</title>



  <link href='//fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.indigo-pink.min.css">
  <link rel="stylesheet" href="styles.css">
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
</head>

<body>
  <header>
   <ul class="w3-navbar w3-margin-bottom w3-blue-grey w3-border w3-large">
    <li><a class="w3-green" href="/profile.php"><i class="fa fa-home w3-large"></i></a></li>
    <li><a href="/upload.php"><i class="fa fa-cloud-upload w3-large"></i></a></li>
    <li><a href="/members.php"><i class="fa fa-group w3-large"></i></a></li>
    <?php 
    // Retrieves $SESSION variables, if user is Admin, display link to AdminPanel
    session_start(); 
    if ($_SESSION["isAdmin"]==1) echo "<li><a href=\"/admin.php\"><i class=\"fa fa-gears w3-large\"></i></a></li>"; 
    ?>
    <li><a href="/index.php"><i class="fa fa-sign-in w3-large"></i></a></li>
    <li class="w3-right w3-small"><div class="w3-container"><p><?="Welcome, ". $_SESSION["username"]?></p></div></li>
  </ul>
</header>
<div class="w3-container">
  <div class="w3-container w3-center w3-pale-blue w3-round-xlarge">
    <h3>Site Members</h3>
  </div>
  <div class="w3-container w3-quarter"><p> </p></div>
  <div class="w3-card-4 w3-white w3-half w3-margin-top">
    <ul class="w3-ul w3-card-4 foll w3-hoverable">

      <?php

      include("config.php");
      $query = "SELECT * FROM `PhotoShare`.`Users`";

      // Runs an SQL Query to display all the users
      $result= mysqli_query($db, $query);

      while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){

        // Computes the number of pictures uploaded by the user
        $num_img =  mysqli_num_rows(mysqli_query($db, "SELECT * FROM `PhotoShare`.`Images` WHERE `username`='" . $row["username"] . "'"));

        // Displays all users and the corresponding number of images they uploaded
        echo "<a href=\"/profile.php?user=" . $row["username"] . "\"><li class=\"w3-padding-hor-16\">
        <img src=\"http://www.w3schools.com/w3css/img_avatar2.png\" class=\"w3-left w3-circle w3-margin-right\" style=\"width:60px\">
        <span class=\"w3-xlarge\">" . $row["username"] . "</span><br>
        <span>" . $num_img . " picture(s)</span>
      </li></a> ";
    }


    ?>
  </ul>
</div>
</div>
</body>
</html>