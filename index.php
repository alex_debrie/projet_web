<?php

include("config.php");

// Destroys potentially existing session
session_unset();
session_destroy();

// Starts a new session
session_start();

// If user filled in the LOGIN form
if (!empty($_POST['login'])) {

	// Retrieves the content of the fields username and password via POST method
	// Improved security (real escape string) to remove potentially dangerous characters from String (backslashes, quotes ...)
	// => Protects against SQL Injections
	$usr = mysqli_real_escape_string($db,$_POST['username']);
	$pass = mysqli_real_escape_string($db,$_POST['password']); 

	// Runs login info against database to check if a corresponding user exists
	$sql = "SELECT * FROM Users WHERE username = '$usr' and password = '$pass'";
	$result = mysqli_query($db,$sql) or die("Query error");
	$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
	$active = $row['active'];

	$count = mysqli_num_rows($result);

	//If there is one result, then the provided login info was correcrt
	if($count == 1) {
		// Sets $SESSION variables
		$_SESSION["username"]=$usr;
		$_SESSION["isAdmin"]=$row["isAdmin"];
		// Redirects user to his profile
		header("location: profile.php");
	}else {
		// Displays error message
		$errlogin = 1;
	}
}

// If user filled in the SIGNIN form
if (!empty($_POST['signin'])) {
	// Retrieves via POST the contents of usr and passwd fields
	$newusr = mysqli_real_escape_string($db,$_POST['newusername']);
	$newpass = mysqli_real_escape_string($db,$_POST['newpassword']);

	// Adds the new user to the database
	$sqlnew = "INSERT INTO `PhotoShare`.`Users` (`username`, `password`, `isAdmin`) VALUES ('$newusr', '$newpass', 0)";
	$newresult = mysqli_query($db,$sqlnew) or die("Query error");
}
?>

<!doctype html>

<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>



	<link href='//fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.indigo-pink.min.css">
	<link rel="stylesheet" href="styles.css">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">

</head>

<body>

	<div class="w3-container">
		<div class="w3-container w3-margin-bottom w3-center w3-pale-blue w3-round-xlarge"><h3>PhotoShare</h3></div>
		<div class="w3-container w3-padding-medium w3-center w3-row">
			<div class="w3-third"><p> </p></div>
			<div class="w3-third w3-card-4 w3-pale-blue">
				<div class="w3-container w3-green"><h3>Login</h3></div>
				<?php 
				// If new user successfully added, displays success message
				if (!empty($_POST['signin']) && ($newresult)) echo "<div class=\"w3-container w3-pale-green w3-bottombar w3-border-green w3-border\"> <p>Account successfully registered !</p> </div>";
				
				// If wrong information was provided, displays error message
				if ($errlogin) echo "<div class=\"w3-container w3-pale-red w3-bottombar w3-border-red w3-border\"> <p>Invalid username or password !</p> </div>";
				?>

				<form method="post">
					<input class="w3-input w3-margin-top" type="text" name="username" placeholder="Username">
					<input class="w3-input w3-margin-bottom" type="password" name="password" placeholder="Password">
					<input class="w3-btn w3-green" value="Login" type="submit" name="login">

				</form>
				<button onclick="document.getElementById('id01').style.display='block'" class="w3-btn w3-teal">New User ?</button>
			</div>
		</div>
	</div>

	<div id="id01" class="w3-modal">
		<div class="w3-container">
			<div class="w3-modal-content w3-animate-top w3-card-8">
				<header class="w3-container w3-teal"> 
					<span onclick="document.getElementById('id01').style.display='none'" 
					class="w3-closebtn">&times;</span>
					<h3>New user ? Sign in !</h3>
				</header>
				<div class="w3-card-4 w3-padding-medium w3-center">
					<form method="post">
						<input class="w3-input" type="text" name="newusername" placeholder="Username">
						<input class="w3-input w3-margin-bottom" type="password" name="newpassword" placeholder="Password">
						<br>
						<input class="w3-btn w3-teal" value="Sign in" type="submit" name="signin">

					</form>
				</div>
			</div>
		</div>
	</div>
</body>

</html>