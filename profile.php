

<!doctype html>

<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Profile</title>



  <link href='//fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.indigo-pink.min.css">
  <link rel="stylesheet" href="styles.css">
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">


</head>
<body>
  <header>
   <ul class="w3-navbar w3-margin-bottom w3-blue-grey w3-border w3-large">
    <li><a class="w3-green" href="/profile.php"><i class="fa fa-home w3-large"></i></a></li>
    <li><a href="/upload.php"><i class="fa fa-cloud-upload w3-large"></i></a></li>
    <li><a href="/members.php"><i class="fa fa-group w3-large"></i></a></li>
    <?php 
    //Retrieves $SESSION variables
    session_start(); 
    // Checks if user is Admin before displaying AdminPanel link
    if ($_SESSION["isAdmin"]==1) echo "<li><a href=\"/admin.php\"><i class=\"fa fa-gears w3-large\"></i></a></li>"; 
    ?>
    <li><a href="/index.php"><i class="fa fa-sign-in w3-large"></i></a></li>
    <li class="w3-right w3-small"><div class="w3-container"><p><?="Welcome, ". $_SESSION["username"]?></p></div></li>
  </ul>
</header>
<div class="w3-container">
  <div class="w3-container w3-center w3-pale-blue w3-round-xlarge"><h3> 
    <?php 
    session_start(); 

    //Checks if user is visiting his own profile or someone else's
    if (isset ($_GET["user"])) {
      $_SESSION["profile"]= "other";
      echo ucfirst($_GET["user"]);
    }
    elseif (!(isset ($_GET["user"]))) {
      $_SESSION["profile"]= "own";
      echo ucfirst($_SESSION["username"]);
    }

    ?> </h3></div>

    <ul class="w3-navbar w3-blue-grey w3-round-xlarge">
      <li><a href="#" class="tablink w3-red" onclick="openTab(event, 'feed');">My Feed</a></li>
      <li><a href="#" class="tablink" onclick="openTab(event, 'albums');">Albums</a></li>
    </ul>

    <div id="feed" class="w3-container tab">
      <h2>Image Feed</h2>

      <div id="myGrid" class="w3-row-padding w3-padding-hor-16 w3-container">
        <!-- <div class="w3-third"> -->

          <?php
          //Retrieves $SESSION variables and MySql connection
          session_start();
          include("config.php");

          //Query for all pictures belonging to a specific user
          $query = "SELECT * FROM `PhotoShare`.`Images` WHERE `username` = '";

          //Shows Private images only if visiting your own profile
          if (isset($_GET["user"])) $query = $query . $_GET["user"] . "'AND `visibility` = 1";
          elseif (!(isset($_GET["user"]))) $query = $query . $_SESSION["username"] . "'";

          $result= mysqli_query($db, $query);

          $count = mysqli_num_rows($result);

          //Displays all corresponding images
          while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
            echo " <a href=\"/photo.php?id=". $row["imageID"] ."\"><img class=\"w3-padding-hor-16\"src=\"". $row["path"] ."\" style=\"width:33%\"></a>";
          }

          ?>
    </div>
  </div>
</div>
<div id="albums" class="w3-container tab">
  <h2>Album : "Favorites"</h2>
  <div class="w3-container">
    <?php
    $fav = "SELECT * FROM `PhotoShare`.`Images` WHERE `username` = '";
    if (isset($_GET["user"])) $fav .= $_GET["user"]."' AND `isFavorite`='1'";
    elseif (!(isset($_GET["user"]))) $fav .= $_SESSION["username"] ."' AND `isFavorite`=1";
    $res= mysqli_query($db, $fav);

     while($row = mysqli_fetch_array($res, MYSQLI_ASSOC)){
            echo " <a href=\"/photo.php?id=". $row["imageID"] ."\"><img class=\"w3-padding-hor-16\"src=\"". $row["path"] ."\" style=\"width:33%\"></a>";
          }
    ?>

<!--     <div class="w3-container w3-quarter w3-center">
      <i class="fa fa-plus-circle w3-jumbo w3-padding-hor-64"></i>
    </div> -->
  </div>
</div> 





<script>
  function openTab(evt, tabName) {
    // Manages tab navigation (between Feed and Album)
    var i, x, tablinks;
    x = document.getElementsByClassName("tab");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " w3-red";
  }
</script>






</div>

</body>
<!-- Clear floats -->
<div class="w3-clear"></div><br><br>
</div>
<script src="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.min.js"></script>
</body>

</html>